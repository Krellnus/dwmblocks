//Modify this file to change what commands output to your statusbar, and recompile using the make command.
//
/* To signal using the kill command it's update signal +34 */
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	/* {"",    "~/bin/statusbar/mycorona.sh",  60,     11}, */
	/* {"",	"music",	0,	11}, */
	{"",	"~/bin/statusbar/weather.sh",	3600,	8},
        {"",    "~/bin/statusbar/updates.sh",   3600,   13},
	{"",	"~/bin/statusbar/light.sh",	2,	6},
	/* {"",	"crypto",	0,	13}, */
	{"",	"~/bin/statusbar/battery.sh",	60,	7},
	/* {"",	"memory",	10,	14}, */
	/* {"",	"cpu",		10,	13}, */
	/* {"",	"moonphase",	18000,	5}, */
	{"",	"~/bin/statusbar/volume.sh",	0,	10},
	{"",	"~/bin/statusbar/rootdisk.sh",	60,	12},
	/* {"",	"nettraf",	1,	16}, */
	{"",	"~/bin/statusbar/homedisk.sh",	60,	5},
	{"",	"~/bin/statusbar/mountdisk.sh",	30,	3},
	{"",	"~/bin/statusbar/ram.sh",	10,	1},
	{"",	"~/bin/statusbar/time.sh",	1,	4},
	{"",	"~/bin/statusbar/date.sh",	180,	15},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
